module.exports = class NumberParser {
    parse(str) {
        return str.match(/\d+/g).map(Number)
    }
}
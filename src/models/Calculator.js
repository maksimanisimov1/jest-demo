const NumberParser = require('./NumberParser')

module.exports = class Calculator {
    constructor(numbers) {
        const numberParser = new NumberParser()

        if (typeof numbers === 'string') {
            this.numbers = numberParser.parse(numbers)
        } else {
            this.numbers = numbers
        }
    }

    sum() {
        return this.numbers.reduce((acc, number) => acc + number, 0)
    }
    
    diff(numbers) {
        return this.numbers.reduce((acc, number, index) => {
            if (index === 0) return number
            return acc - number
        }, this.numbers[0])
    }
}
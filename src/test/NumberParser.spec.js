const NumberParser = require('../models/NumberParser')

describe('NumberParser', () => {
    describe('parse', () => {
        test('returns array of extracted numbers from given string', () => {
            const numberParser = new NumberParser()

            expect(numberParser.parse('15,sdss14')).toEqual([15, 14])
        })
    })
})
const Calculator = require('../models/Calculator')


jest.mock('../models/NumberParser', () => {
    return jest.fn().mockImplementation(() => {
        return {
            parse: jest.fn(() => [20, 5, 3])
        }
    })
})

describe('Calculator', () => {
    describe('sum', () => {
        test('returns sum of given numbers', () => {
            const calculator = new Calculator([5, 2, 3])
            expect(calculator.sum()).toBe(10)
        })

        test('returns sum of numbers in given string', () => {
            const calculator = new Calculator('5a5b3c')
            expect(calculator.sum()).toBe(28)
        })
    })

    describe('diff', () => {
        test('returns difference of given numbers', () => {
            const calculator = new Calculator([15, 5, 3])
            expect(calculator.diff()).toBe(7)
        })

        test('returns difference of numbers in given string', () => {
            const calculator = new Calculator('10a5b3c')
            expect(calculator.diff()).toBe(12)
        })
    })
})